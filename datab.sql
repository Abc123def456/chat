drop database if exists chat;
create database chat;
use chat;

create table utente(
id integer not null primary key auto_increment,
nome varchar(150) not null,
cognome varchar(150) not null,
userName varchar(20) not null unique,
pass varchar(20) not null,
tipo varchar(10) not null CONSTRAINT ruoloUte CHECK (tipo = "semplice" OR tipo = "admin")
);
create table messaggio(
id integer not null primary key auto_increment,
corpo text not null,
dataOra timestamp not null,
idUser integer not null,
foreign key (idUser) references utente(id) on delete cascade
);

insert into utente(nome, cognome, userName, pass,tipo) value ("Andrea", "Caria", "andreaC", "123456","semplice");
insert into utente(nome, cognome, userName, pass,tipo) value ("Marisa", "Mastroleo", "MarisMas", "abcdef","semplice");
insert into utente(nome, cognome, userName, pass,tipo) value ("Carlo", "Carloni", "CrlC", "uytt","admin");

insert into messaggio(corpo,dataOra, idUser) value ("ciao!!","2021-03-11 10:00:00",1);
insert into messaggio(corpo,dataOra, idUser) value ("ciao!!","2021-03-11 10:00:30",2);
insert into messaggio(corpo,dataOra, idUser) value ("Come stai?","2021-03-11 10:01:00",1);
insert into messaggio(corpo,dataOra, idUser) value ("Tutto bene tu?!!","2021-03-11 10:02:00",2);

select * from messaggio;

select* from utente;