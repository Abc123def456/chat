<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%
 
  		HttpSession sessione = request.getSession();
    	//String ruolo_utente = (String)sessione.getAttribute("role") != null ? (String)sessione.getAttribute("role") : "";
    	
    	//Metodo equivalente all'operatore ternario
    	String ruolo = "";
    	String user = "";
    	if((String)sessione.getAttribute("role") != null){
    		ruolo = (String)sessione.getAttribute("role");
    	}
    	if((String)sessione.getAttribute("user") != null){
    		user = (String)sessione.getAttribute("user");
    	}
    	
    	if(!ruolo.equals("admin") && !ruolo.equals("semplice"))
    		response.sendRedirect("index.html");
    %>  
    
    
    
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Chat</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"> 

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">


	
</head>
<body>

	<div class="container">
	   
	
		<div class="row m-5">
			<div class="col">
				<h4>Chat:"M&A"</h4>
				<div class="card">
  					<div id= "cardHeaderNome" class="card-header" data-username="<% out.print(user); %>" data-role="<% out.print(ruolo); %>">
  					<div class = "row">
    					<div class = " p-0" style = "width: 30%; min-width: 70px; "> 
    					<img  src="https://picsum.photos/200"  width="70px" height="70px" style="border-radius: 100%;">
    					</div>
    					<% if(ruolo.equals("semplice")){
    						%>
    						<div id= "cardUtente" class = " p-0" style = "width: 69%;"> 
    						</div>
    						<%	
    					} if(ruolo.equals("admin")){
    						
    						%>
    						<div id= "cardAdmin" class = " p-0" style = "width: 69%;"> 
    						</div>
    						<%	
    						
    					}
    						
    					
    					%>
    					
    						
    						
    						
    					
    				</div>
  					</div>
  					<div class="card-body" id ="mess_body">
    					
  					</div>
  						<div class="card-footer ">
							<div class="input-group" style = "width:100%" >
								<div class="input-group-prepend" style = "width:88%" >
							    	<textarea id="message" class="form-control"  aria-label="With textarea"></textarea>
							  	</div>
							  	<div class ="m-1" style = "width:10% ; text-align: right;" >
								    <button style = " border-radius: 100%; " data-user = "<% out.print(user);%>"id = "send" class="btn btn-outline-success" type="button" onclick= "send(this)"> <i class="fa fa-paper-plane"></i></button>
								</div>
							
							</div>
  						</div>
				</div>
			</div>
		</div>

	</div>
	
<div id = "modaleEliminazione" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Elimina Utente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vuoi eliminare l'utente?</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-outline-warning" data-dismiss="modal">Annulla</button>
        <button type="button" class="btn btn-danger" data-username="<% out.print(user); %>"  onclick= "deleteUtente(this)">Elimina</button>
        
      </div>
    </div>
  </div>
</div>

	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="JS/chatJS.js"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>
</html>