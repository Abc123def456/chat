/**
 * 
 */

$(document).ready(

	function(){
		let utente = $("#cardHeaderNome").data("username");
			let ruolo = $("#cardHeaderNome").data("role");
			if(ruolo=="semplice"){
				createcard(utente);
			}
			if(ruolo=="admin"){
				createcardAdmin(utente);
			}
		
		
		recuperaMess(utente);
		
		window.setInterval(
					function(){
						recuperaMess(utente);
						console.log("AGGIORNATO");
					}
			, 3000);
			
		
		
	}
	
	
)
function aprimodale(){
	$("#modaleEliminazione").toggle();
	
}

function createcardAdmin(utente){

	let content = '<div class="card text-left" pl-0 style = "max-height: 70px"> <div class="card-body">';
	content += "ADMIN: "+utente+"";
	content += '<button class= "btn btn-outline-danger ml-2" onclick = "eliminamess()"> elimina tutti i messaggi!</button>'
	content += '</div></div>';
	$("#cardAdmin").html(content);
	}
	
	
function createcard(utente){

	let content = '<button style= "padding: 0px; border:0px;" onclick = "aprimodale()" >'
	content += '<div class="card text-left" pl-0 style = "max-height: 70px"> <div class="card-body">';
	content += ""+utente+"";
	content += "</br> <p style = 'font-size:60%'>Cliccami per apertura dettagli utente</p>"
	content += ' </div></div></button>';
	$("#cardUtente").html(content);
	
	
}
function eliminamess(){
	$.ajax(
				{
					url: "http://localhost:8080/ChatSe/DeleteMessaggi",
					method: "POST",
					
					success: function() {
						Swal.fire("Eliminazione avvenuta con sucesso!");
						window.setInterval(2000);
						window.location.replace("index.html");
						//Swal.fire("inserito");
						
					},
					error: function(){
						
						Swal.fire("errore");
						
					}
					}
				);
	
	
}

function deleteUtente(objButton){
	
	let utente =  $(objButton).data("username");

		$.ajax(
				{
					url: "http://localhost:8080/ChatSe/DeleteUtente",
					method: "POST",
					data: {
							user: utente,
							
					},
					success: function() {
						Swal.fire("Eliminazione avvenuta con sucesso!");
						window.setInterval(2000);
						window.location.replace("index.html");
						//Swal.fire("inserito");
						
					},
					error: function(){
						
						Swal.fire("errore");
						
					}
					}
				);
	
}

function paddy(num, padlen, padchar) {
    var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
    var pad = new Array(1 + padlen).join(pad_char);
    return (pad + num).slice(-pad.length);
}

function send(objbutton){
			    let utente = $(objbutton).data("user");
				let newMess = $("#message").val();
				$("#message").val("");
				
				$.ajax(
				{
					url: "http://localhost:8080/ChatSe/inseriscimessaggio",
					method: "POST",
					data: {
							user: utente,
							messaggio: newMess
					},
					success: function() {
						recuperaMess(utente);
						//Swal.fire("inserito");
						
					},
					error: function(){
						
						Swal.fire("errore");
						
					}
					}
				);
				
				
			}
function recuperaMess(utente){
	
	$.ajax(
				{
					url: "http://localhost:8080/ChatSe/RecuperaMessaggi",
					method: "POST",
					
					success: function(messaggi) {
						console.log(messaggi);
						costruisciMess(messaggi,utente);
						
					},
					error: function(){
						
						Swal.fire("errore");
						
					}
					}
				);
	
}
function costruisciMess(mess,utente){
	let contenuto = "";
	for(let i=0; i<mess.length; i++){
		if (mess[i].user.username == utente)
				contenuto += cardMess(mess[i]);
		else 
				contenuto += cardMessAltri(mess[i]);
	}
	
	$("#mess_body").html(contenuto);
}

function cardMess(singlemess)
{	console.log(singlemess.dataOra);
	let riga = "";
	
	riga += '<div class="d-flex justify-content-end">';
	riga += '<div class="card mt-1 mb-1" style="max-width: 70%;">';
	riga += '<div class="card-header p-0 pl-1 pr-1 border-warning" style="font-size:60%;" >';
	riga +=  ""+ singlemess.user.nome +"";
	riga += '</div>';
	riga += '<div class="card-body p-1 pl-3 pr-3">';
	riga += ""+singlemess.corpo+"";
	riga += '</div>';
	riga += '<div class="card-footer p-0 pr-1 text-muted text-right" style="font-size:60%; background-color: white">';
	riga+= ''+paddy(singlemess.dataOra.time.hour,2)  +':'+ paddy(singlemess.dataOra.time.minute,2);
	riga +='</div>';
	riga += '</div>';
	riga += '</div>';
	return riga;
	}
	
function cardMessAltri(singlemess)
{
	let riga = "";
	
	riga += '<div class="d-flex justify-content-start">';
	riga += '<div class="card mt-1 mb-1" style="max-width: 70%;">';
	riga += '<div class="card-header p-0 pl-1 pr-1  border-danger" style="font-size:60%;" >';
	riga +=  ""+ singlemess.user.nome +"";
	riga += '</div>';
	riga += '<div class="card-body p-1 pl-3 pr-3">';
	riga += ""+singlemess.corpo+"";
	riga += '</div>';
	riga += '<div class="card-footer p-0 pl-1 text-muted text-left" style="font-size:60%; background-color: white">';
	riga+= ''+paddy(singlemess.dataOra.time.hour,2) +':'+paddy(singlemess.dataOra.time.minute,2);
	riga +='</div>';
	riga += '</div>';
	riga += '</div>';
	return riga;
	}