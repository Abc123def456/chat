$(document).ready(
	function() {
		console.log("ciao");
		$("#login").click(
			function() {
				let user = $("#inputUser").val();
				let password = $("#inputPass").val();
				if (controlloInput(user) && controlloInput(password)) {
					console.log("hey you");
					console.log()
					$.ajax(
						{
							url: "http://localhost:8080/ChatSe/verificalogin",
							method: "POST",
							data: {
								inputUser: user,
								inputPass: password
							},
							success: function(utenteLog) {
								console.log(utenteLog);
								if (utenteLog.role == "semplice" || utenteLog.role == "admin") {
									window.location.replace("chat.jsp");
								}
								else {
									Swal.fire("username o password errati");
									window.location.replace("index.html");
								}
							},
							error: function(risultato) {
								console.log(risultato);
								Swal.fire("username o password errati");
							}
						}
					);
				}
				else
					Swal.fire("Uno dei campi e' vuoto");
			}
		)
		$("#registrati").click(
			function() {
				console.log("ciao dal bottone");
				let newNome = $("#newNome").val();
				let newSurname = $("#newSurname").val();
				let newUsername = $("#newUsername").val();
				let newPassword = $("#newPassword").val();
				let newPasswordConferma = $("#newPasswordConferma").val();
				let tipol = "semplice";
				if (controlloInput(newNome) &&
					controlloInput(newSurname) &&
					controlloInput(newUsername) &&
					controlloInput(newPassword) &&
					controlloInput(newPasswordConferma)) {
					if (newPassword == newPasswordConferma) {
						Swal.fire("Utente corretto da inserire");
						$.ajax(
							{
								url: "http://localhost:8080/ChatSe/InserisciUtente",
								method: "POST",
								data: {
									newNome1: newNome,
									newSurname1: newSurname,
									newUsername1: newUsername,
									newPassword1: newPassword,
									tipol1: tipol
								},
								success: function(risultato) {
									console.log("utente inserito con successo");
									window.location.replace("chat.jsp");
								},
								error: function(risultato) {
									console.log(risultato);
									Swal.fire("username o password errati");
								}
							}
						);
					}
					else {
						Swal.fire("Le due password non coincidono!");
					}
				}
				else
					Swal.fire("Uno dei campi e' vuoto");
			}
		);
	}
);
function controlloInput(nome) {
	if (nome.trim().length == 0) {
		return false;
	}
	return true;
}