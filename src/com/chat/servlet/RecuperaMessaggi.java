package com.chat.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.chat.classi.Messaggio;
import com.chat.services.MessaggioDAO;
import com.chat.services.UtenteDAO;
import com.google.gson.Gson;



/**
 * 
 * Servlet dedicata al recupero da database di tutti i messaggi con i 
 * relativi user che li hanno inviati i quali verranno poi visualizzati 
 * nella schermata di chat
 *
 */
@WebServlet("/RecuperaMessaggi")
public class RecuperaMessaggi extends HttpServlet {
    public RecuperaMessaggi() {
      
    }
 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doPost(request,response);
 }
 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  HttpSession sessione = request.getSession();
  PrintWriter out = response.getWriter();
  response.setContentType("application/json");
  MessaggioDAO mesDao = new MessaggioDAO();
  UtenteDAO uteDao = new UtenteDAO();
  try {
    ArrayList<Messaggio> elenco = mesDao.getAll();
    for (Messaggio iMess: elenco) {
    	iMess.setUser(uteDao.getById(iMess.getIdUser()));
    }
    String risultatoJson = new Gson().toJson(elenco);
    out.print(risultatoJson);
  } catch (SQLException e) {
   out.print (e.getMessage());
  }
 }
}