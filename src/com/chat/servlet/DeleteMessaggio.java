package com.chat.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.chat.services.MessaggioDAO;

/**
 * Servlet implementation class DeleteMessaggio
 * dedicata alla eliminazione del singolo messaggio tramite id messaggio 
 * (non implementata nel frontend)
 * 
 */
@WebServlet("/DeleteMessaggio")
public class DeleteMessaggio extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DeleteMessaggio() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String idmes = request.getParameter("idmes");
		int idmess = Integer.valueOf(idmes);
		
		MessaggioDAO mesdao= new MessaggioDAO();
		
		try {
			
			if(mesdao.deleteone(idmess))
			{
				System.out.println("Utente eliminato con successo");
			}
			else
			{
				System.out.println("Eliminazione non effettuata");
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
