package com.chat.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.chat.classi.Utente;
import com.chat.services.MessaggioDAO;
import com.chat.services.UtenteDAO;

/**
 * Servlet adibita alla delete di tutti i messaggi in chat
 * pu� essere effettuata solo da admin
 *
 */
@WebServlet("/DeleteMessaggi")
public class DeleteMessaggi extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DeleteMessaggi() {
        super();

    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sessione = request.getSession();
		String role = (String)sessione.getAttribute("role") != null ? (String)sessione.getAttribute("role") : "";
		
		if(role.equals("admin")) {

		MessaggioDAO mesdao=new MessaggioDAO();
		
		try {
			if(mesdao.deleteAll())
			{
				System.out.println("Messaggi eliminati con successo");
			}
			else
			{
				System.out.println("Eliminazione non effettuata");
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		}
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
	}

}
