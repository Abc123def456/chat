package com.chat.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.chat.classi.Utente;
import com.chat.services.UtenteDAO;

@WebServlet("/DeleteUtente")
public class DeleteUtente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * Ogni utente pu� scegliere di eliminare il proprio account
	 * Una volta loggato, accede alla pagina di chat ove vi � un button legato alla eliminazione del proprio
	 * profilo tramite user il quale � unique
	 * 
	 */
    public DeleteUtente() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String user = request.getParameter("user");

		UtenteDAO utdao= new UtenteDAO();
		
		try {
			Utente tem =utdao.getByUser(user);
			if(utdao.delete(tem))
			{
				System.out.println("Utente eliminato con successo");
			}
			else
			{
				System.out.println("Eliminazione non effettuata");
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
