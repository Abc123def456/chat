package com.chat.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.chat.classi.Utente;
import com.chat.services.UtenteDAO;

/**
 * Servlet implementation class InserisciUtente
 * Servlet adibita, in fase di registrazione alla memorizzazione dell'utente nel database
 * in fase di registrazione l'utente pu� registrarsi solo come simple
 * 
 */
@WebServlet("/InserisciUtente")
public class InserisciUtente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InserisciUtente() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String varNome = request.getParameter("newNome1");
		String varCognome = request.getParameter("newSurname1");
		String varUser = request.getParameter("newUsername1");
		String varPass = request.getParameter("newPassword1");
		String varTipo = request.getParameter("tipol1");
		
		if(!varNome.isEmpty() && !varCognome.isEmpty() && !varUser.isEmpty()) {
			
			Utente temp = new Utente();
			temp.setNome(varNome);
			temp.setCognome(varCognome);
			temp.setUsername(varUser);
			temp.setPassword(varPass);
			temp.setTipo(varTipo);
			
			UtenteDAO utdao = new UtenteDAO();
			try {
				utdao.insert(temp);
				
			} catch (SQLException e) {
				System.out.println("ERRORE DI SALVATAGGIO: " + e.getMessage());
			}
			
			
		}
		else {
			doGet(request, response);
		}
		
	}

}
