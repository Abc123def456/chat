package com.chat.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.chat.classi.Messaggio;
import com.chat.classi.Utente;
import com.chat.services.MessaggioDAO;
import com.chat.services.UtenteDAO;
import com.google.gson.Gson;

/**
 * Servlet implementation class InserisciMessaggio
 * Dedicata alla insert del messaggio all'interno del database
 * oltre al messaggio memorizzo lo user che lo ha inviato
 * 
 */
@WebServlet("/inseriscimessaggio")
public class InserisciMessaggio extends HttpServlet {
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InserisciMessaggio() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		MessaggioDAO mesDao = new MessaggioDAO();
		UtenteDAO uteDao = new UtenteDAO();
		
		String user = request.getParameter("user");
		String textMess = request.getParameter("messaggio");
		
		Messaggio mess = new Messaggio();
		try {
			Utente itemp = uteDao.getByUser(user);
			int idUser = itemp.getUtenteID();
			mess.setCorpo(textMess);
			mess.setIdUser(idUser);
			mess.setOrario(LocalDateTime.now());
			mesDao.insert(mess);
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Errore in inserimento messaggio: "+e.getMessage());
		}
		
		
	}

}