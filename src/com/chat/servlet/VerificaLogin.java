package com.chat.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.chat.classi.Utente;
import com.chat.classi.UtenteConnesso;
import com.chat.services.UtenteDAO;
import com.google.gson.Gson;

/**
 * Servlet dedicata alla verifica delle credenziali degli utenti
 *
 */

@WebServlet("/verificalogin")
public class VerificaLogin extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("index.html");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String user = request.getParameter("inputUser") != null ? request.getParameter("inputUser") : "";
		String pass = request.getParameter("inputPass") != null ? request.getParameter("inputPass") : "";
		Utente utente = new Utente();
		UtenteConnesso uteCon = new UtenteConnesso();
		Gson Json = new Gson();
		utente.setUsername(user);
		utente.setPassword(pass);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		UtenteDAO utente_dao = new UtenteDAO();
		try {
			utente_dao.checkCredentials(utente);
			if (utente.getUtenteID() != null) {
				HttpSession sessione = request.getSession();
				if (utente.getTipo().equals("admin")) {
					sessione.setAttribute("role", "admin");
					sessione.setAttribute("user", user);
					
					uteCon.setRole("admin");
					uteCon.setUser(user);
					String elencoCatJson = Json.toJson(uteCon);
					out.print(elencoCatJson);
					return;
				}
				if (utente.getTipo().equals("semplice")) {
					sessione.setAttribute("role", "semplice");
					sessione.setAttribute("user", user);
					uteCon.setRole("semplice");
					uteCon.setUser(user);
					String elencoCatJson = Json.toJson(uteCon);
					out.print(elencoCatJson);
					
					return;
				}
			
			} else {
				uteCon.setRole(null);
				uteCon.setUser(null);
				String elencoCatJson = Json.toJson(uteCon);
				out.print(elencoCatJson);
				
				return;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} 
	}
}
