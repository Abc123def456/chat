package com.chat.classi;

public class UtenteConnesso {
	private String user;
	private String role;
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public UtenteConnesso(String user, String role) {
		super();
		this.user = user;
		this.role = role;
	}
	
	public UtenteConnesso() {
		
	}
}
