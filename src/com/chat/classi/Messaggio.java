package com.chat.classi;



import java.time.LocalDateTime;

public class Messaggio {

	private Integer id;
	private Integer idUser;
	private String corpo;
	private LocalDateTime dataOra;
	private Utente user;
	
	public LocalDateTime getDataOra() {
		return dataOra;
	}

	public void setDataOra(LocalDateTime dataOra) {
		this.dataOra = dataOra;
	}

	


	public Utente getUser() {
		return user;
	}

	public void setUser(Utente user) {
		this.user = user;
	}

	public Messaggio() {
		
	}

	public Messaggio(String corpo, LocalDateTime orario) {
		super();
		this.corpo = corpo;
		this.dataOra = orario;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer messaggioID) {
		this.idUser = messaggioID;
	}

	public String getCorpo() {
		return corpo;
	}

	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}

	public LocalDateTime getOrario() {
		return dataOra;
	}

	public void setOrario(LocalDateTime orario) {
		this.dataOra = orario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

}