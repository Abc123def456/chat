package com.chat.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.chat.classi.Messaggio;
import com.chat.classi.Utente;
import com.chat.connessione.ConnettoreDB;
import com.chat.services.DAO;

public class MessaggioDAO implements DAO<Messaggio>{

	@Override
/**
 * 
 */
	public ArrayList<Messaggio> getAll() throws SQLException {
		ArrayList<Messaggio> elenco = new ArrayList<Messaggio>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT corpo, dataOra, idUser FROM messaggio ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Messaggio temp = new Messaggio();
       		temp.setCorpo(risultato.getString(1));
       		temp.setOrario(risultato.getTimestamp(2).toLocalDateTime());
       		temp.setIdUser(risultato.getInt(3));
       		
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Messaggio t) throws SQLException {
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO messaggio (corpo, dataOra, idUser) VALUES (?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getCorpo());
        ps.setTimestamp(2, Timestamp.valueOf(t.getOrario()));
       	ps.setInt(3, t.getIdUser());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
		
	}

	public boolean deleteone(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM messaggio WHERE id = ? ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}
	
	public boolean deleteAll() throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM messaggio";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);     	
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean delete(Messaggio t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	


}
