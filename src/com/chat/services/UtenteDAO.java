package com.chat.services;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.chat.classi.Utente;
import com.chat.connessione.ConnettoreDB;



public class UtenteDAO implements DAO<Utente> {

	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		ArrayList<Utente> elenco = new ArrayList<Utente>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT nome,cognome,userName,pass FROM utente ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Utente temp = new Utente();
       		temp.setNome(risultato.getString(1));
       		temp.setCognome(risultato.getString(2));
       		temp.setUsername(risultato.getString(3));
       		temp.setPassword(risultato.getString(4));
       		
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Utente t) throws SQLException {
	     Connection conn = ConnettoreDB.getIstanza().getConnessione();
	     String query = "INSERT INTO utente (nome,cognome,userName,pass,tipo) VALUES (?,?,?,?,?)";
	        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
	        ps.setString(1, t.getNome());
	        ps.setString(2, t.getCognome());
	        ps.setString(3, t.getUsername());
	        ps.setString(4, t.getPassword());
	        ps.setString(5, t.getTipo());
	        ps.executeUpdate();
	        ResultSet risultato = ps.getGeneratedKeys();
	        risultato.next();
	        t.setUtenteID(risultato.getInt(1));
	 }

	@Override
	public boolean delete(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM utente WHERE id = ? ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getUtenteID());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}
	
	public Utente getByUser(String Username) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT id, nome,cognome,userName,pass FROM utente WHERE userName = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, Username);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Utente temp = new Utente();
   		temp.setUtenteID(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setCognome(risultato.getString(3));
   		temp.setUsername(risultato.getString(4));
   		temp.setPassword(risultato.getString(5));
   		
       	return temp;
	}
	public void checkCredentials(Utente objUtente) throws SQLException {
		  
	     	Connection conn = ConnettoreDB.getIstanza().getConnessione();
	     	String query = "SELECT id, userName, pass ,tipo FROM utente WHERE userName = ? AND pass = ?";
	        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
	        ps.setString(1, objUtente.getUsername());
	        ps.setString(2, objUtente.getPassword());
	        ResultSet risultato = ps.executeQuery();
	        risultato.next();
	        objUtente.setUtenteID(risultato.getInt(1));
	        objUtente.setTipo(risultato.getString(4));
	 }
	
	public Utente getById(int Id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT id, nome,cognome,userName, pass FROM utente WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, Id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Utente temp = new Utente();
   		temp.setUtenteID(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setCognome(risultato.getString(3));
   		temp.setUsername(risultato.getString(4));
   		temp.setPassword(risultato.getString(5));
   		
       	return temp;
	}

}
